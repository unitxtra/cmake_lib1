//
// Created by Anders Cedronius on 2020-04-21.
//

#include "SomeClass.h"

int SomeClass::addTwoNumbers(int number1, int number2) {
    return number1 + number2;
}

int SomeClass::subtractFive(int number) {
    return number - 5;
}