//
// Created by Anders Cedronius on 2020-04-21.
//

#ifndef CMAKE_LIB1_SOMECLASS_H
#define CMAKE_LIB1_SOMECLASS_H


class SomeClass {
public:
    int addTwoNumbers(int number1, int number2);
    int subtractFive(int number);
};


#endif //CMAKE_LIB1_SOMECLASS_H
